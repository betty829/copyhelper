import unittest
from copy_Helper.CopyHelper import CopyFactory
from copy_Helper.CopyHelper import CampoSwitch

class test_copy(unittest.TestCase):
    def setUp(self):
        self.linea=CopyFactory()
  
    def test_crearCampo(self):
        self.assertIsInstance(
            self.linea.crearCampo("88 SL88F-CTA-AHORROS         VALUE     '1'."),
            CampoSwitch
            )

    def test_validarArchivo(self):
        self.assertTrue(self.linea.validarArchivo("B:/Estudio AvVillas/Python/CopyHelper/SLF531.cpy"),"True")
        self.assertFalse(self.linea.validarArchivo("B:/Estudio AvVillas/Python1/CopyHelper/SLF531.cpy"),"False")

    
