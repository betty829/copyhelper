from CampoAbstracto import CampoAbstracto 
from CampoSwitch import CampoSwitch
from CampoNormal import CampoNormal
from CampoSuperior import CampoSuperior
class CampoBuilder:
    def crearCampo(self, linea):
        print("linea1 es:",linea)
        linea=linea[6:]
        linea_sin_espacio=linea.strip()
        print("linea2 es:",linea)
        if linea_sin_espacio[0:2]=='88':
            return CampoSwitch()
        else:    
            if " PIC " in linea_sin_espacio:
                return CampoNormal()
            else:
                return CampoSuperior()

campoBuilder=CampoBuilder()
campo=campoBuilder.crearCampo("030310              88 SL88F-CTA-AHORROS         VALUE     '1'.")    
print(type(campo))            