import os.path
from .CampoAbstracto import CampoAbstracto
from .CampoSwitch import CampoSwitch
from .CampoNormal import CampoNormal
from .CampoSuperior import CampoSuperior

class CopyFactory:
    rutaArchivo="B:/Estudio AvVillas/Python/CopyHelper/SLF531.cpy"

    def __init__(self):
        self.raizCopy=[]

    def crearCampo(self, linea):
#        linea_sin_espacio=linea.strip()
        if linea[0:2]=='88':
            return CampoSwitch()
        else:    
            if " PIC " in linea:
                return CampoNormal()
            else:
                return CampoSuperior()

    def validarArchivo(self,rutaArchivo):
        if os.path.exists(rutaArchivo):
            return True
        else:
            print("El fichero NO existe")
            return False
            
    def procesarCopy(self):
        if self.validarArchivo(CopyFactory.rutaArchivo):
            archivo=open(CopyFactory.rutaArchivo)
            for linea in archivo:
                linea=linea[6:]
                linea_sin_espacio=linea.strip()
                if linea_sin_espacio[0:1]=='*':                    
                    continue
                elif not linea_sin_espacio.strip():
                    continue
                elif linea_sin_espacio[0:2]=='SL':
                    continue
                else:
                    tipoCampo=self.crearCampo(linea_sin_espacio)
                    print("linea es:", linea_sin_espacio)
                    print(type(tipoCampo))
                    self.raizCopy.append(tipoCampo)
            archivo.close()


copy=CopyFactory()
copy.procesarCopy()
print(copy.raizCopy)
