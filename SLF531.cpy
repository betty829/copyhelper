      *---------------------------------------------------------------*
      *                        DATOS  COMUNES                         *
      *---------------------------------------------------------------*
           02 SL88-ENTRADA.
              03 SL88-LLAVE-TRAMA.
                 05 SL88-TRANS-CICS              PIC X(04).
                 05 SL88-COD-OFIC                PIC 9(03).
                 05 SL88-FEC-TRANS               PIC 9(08).
                 05 SL88-JORN-TRANS              PIC X(01).
                 05 SL88-TIPO-OFIC               PIC 9(01).
                 05 SL88-FUNC-TIMBRE             PIC 9(13).
                 05 SL88-SEC-TRANS               PIC 9(06).
                 05 SL88-SEC-DOC                 PIC 9(04).
              03 SL88-DATOS-TRANSACCION.
                 05 SL88-COD-TRANS               PIC 9(06).
                 05 SL88-HORA-TRANS              PIC 9(06).
                 05 SL88-ESTADO-TRANS            PIC X(01).
                    88 SL88-NORMAL               VALUE 'N'.
                    88 SL88-REVERSO              VALUE 'R'.
                 05 SL88-MODO-ENT-TRANS          PIC X(01).
                    88 SL88-LINEA                VALUE 'L'.
                    88 SL88-REENTRADA            VALUE 'A'.
                 05 SL88-FEC-COMP                PIC 9(08).
                 05 SL88-FEC-COMP-R   REDEFINES  SL88-FEC-COMP.
                    07 SL88-ANO-COMP             PIC 9(04).
                    07 SL88-MES-COMP             PIC 9(02).
                    07 SL88-DIA-COMP             PIC 9(02).
                 05 SL88-FEC-APLIC               PIC 9(08).
              03 SL88-DATOS-FUNC.
                 05 SL88-FUNC-AUX                PIC 9(13).
                 05 SL88-CLAVE-FUNC-AUX          PIC X(16).
                 05 SL88-FUNC-AUTORIZA           PIC 9(13).
                 05 SL88-CLAVE-FUNC-AUTORIZ      PIC X(16).
                 05 SL88-COD-AUTORIZ             PIC 9(04).
              03 SL88-DATOS-CANAL.
                 05 SL88-TIPO-DE-CANAL           PIC X(01).
                    88 SL88-OFICINA              VALUE 'O'.
                    88 SL88-POS                  VALUE 'P'.
                    88 SL88-ATM                  VALUE 'A'.
                 05 SL88-COD-TERMINAL            PIC X(08).
                 05 SL88-DIR-IP-ESTACION         PIC X(13).
                 05 SL88-VERS-SOFTW-CANAL        PIC 9(02).
                 05 SL88-NATURALEZA-TRANS        PIC X(01).
              03 SL88-DATOS                      PIC X(867).

      *---------------------------------------------------------------*
      *               DATOS MENSAJERIA FINANCIERA                     *
      *---------------------------------------------------------------*
              03 SL88F-DATOS-FINANCIEROS REDEFINES  SL88-DATOS.
                 05 SL88F-DATOS-PRODUCTO-ORIGEN.
                    07 SL88F-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88F-COD-PROD-ORIG       PIC 9(02).
                    07 SL88F-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88F-PROD-ORIG-R REDEFINES SL88F-NRO-PROD-ORIG.
                       09 SL88F-PROD-ORIG        PIC 9(15).
                       09 SL88F-DIGITO           PIC 9(01).
                    07 SL88F-TRACK-2             PIC X(37).
                    07 SL88F-CLAVE-TARJ          PIC X(16).
                    07 SL88F-NRO-DE-DOC          PIC 9(10).
                    07 SL88F-IND-MEDIO-ACCESO    PIC X(01).
                    07 SL88F-NRO-DOCS            PIC 9(04).
                 05 SL88F-DATOS-VALORES.
                    07 SL88F-VLR-EFECTIVO        PIC 9(16)V99.
                    07 SL88F-VLR-CHEQUE          PIC 9(16)V99.
                    07 SL88F-VLR-TOTAL           PIC 9(16)V99.
                 05 SL88F-DATOS-PROD-DESTINO.
                    07 SL88F-ENTIDAD-DEST        PIC 9(02).
                    07 SL88F-COD-PROD-DEST       PIC 9(02).
                    07 SL88F-NRO-PROD-DEST       PIC 9(16).
                 05 SL88F-DATOS-CHEQUE.
                    07 SL88F-NRO-CHEQUE          PIC 9(10).
                    07 SL88F-BCO-CHEQUE          PIC 9(02).
                    07 SL88F-BENEFICIARIO        PIC X(30).
                 05 SL88F-REFERENCIAS.
                    07 SL88F-REF1                PIC 9(16).
                    07 SL88F-REF2                PIC 9(16).
                    07 SL88F-REF3                PIC X(16).
                    07 SL88F-REF4                PIC X(16).
                    07 SL88F-VLR-ADIC1           PIC 9(16)V99.
021107              07 SL88F-IDENT-1 REDEFINES   SL88F-VLR-ADIC1.
021107                 09 SL88F-IDENT1           PIC 9(13).
021107                 09 SL88F-ID-TIPO1         PIC 9(1).
021107                 09 SL88F-ROL1             PIC 9(2).
021107                 09 SL88F-FILLER1          PIC X(2).
                    07 SL88F-VLR-ADIC2           PIC 9(16)V99.
021107              07 SL88F-IDENT-2 REDEFINES   SL88F-VLR-ADIC2.
021107                 09 SL88F-IDENT2           PIC 9(13).
021107                 09 SL88F-ID-TIPO2         PIC 9(1).
021107                 09 SL88F-ROL2             PIC 9(2).
021107                 09 SL88F-FILLER2          PIC X(2).
                    07 SL88F-VLR-ADIC3           PIC 9(16)V99.
                    07 SL88F-VLR-ADIC4           PIC 9(16)V99.
                    07 SL88F-VLR-ADIC5           PIC 9(16)V99.
                 05 SL88F-INDICADORES-MOTIVOS.
                    07 SL88F-MOTIVO1             PIC 9(04).
                    07 SL88F-MOTIVO2             PIC 9(04).
                    07 SL88F-IND-PAGO            PIC 9(02).
                    07 SL88F-IND-PAGO-R   REDEFINES SL88F-IND-PAGO.
                       09 SL88F-TIP-BLOQ         PIC X(02).
                 05 SL88F-DATOS-CONTABLES.
                    07 SL88F-BCO-ORIG            PIC 9(02).
                    07 SL88F-BCO-DEST            PIC 9(02).
                    07 SL88F-CTRO-COSTO          PIC X(04).
                    07 SL88F-DETALLE             PIC X(20).
                    07 SL88F-MOTIVO-DEVOL-CHEQ   PIC 9(02).
                    07 SL88F-IND-TRANSFER        PIC 9(02).
                    07 SL88F-FEC-E               PIC 9(08).
030310           05 SL88F-TIPO-CUENTA            PIC       X(1).
030310              88 SL88F-CTA-AHORROS         VALUE     '1'.
030310              88 SL88F-CTA-CORRIENTE       VALUE     '2'.
090603           05 SL88F-DATOS-CHEQUE-GERENCIA OCCURS 9 TIMES.
090603              07 SL88F-NRO-CHEQUE-GER      PIC 9(10).
090603              07 SL88F-VLR-CHEQUE-GER      PIC 9(16)V99.
090603*          05 FILLER                       PIC X(34).
090603           05 FILLER                       PIC X(206).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 1 (APERTURAS)          *
      *---------------------------------------------------------------*
              03 SL88A-DATOS-APERTURAS REDEFINES SL88-DATOS.
                 05 SL88A-DATOS-PRODUCTO-ORIGEN.
                    07 SL88A-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88A-COD-PROD-ORIG       PIC 9(02).
                    07 SL88A-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88A-PROD-ORIG-R  REDEFINES SL88A-NRO-PROD-ORIG.
                       09 SL88A-PROD-ORIG        PIC 9(15).
                       09 SL88A-DIGITO           PIC 9(1).
                    07 SL88A-COD-SUB-PROD        PIC X(01).
                 05 SL88A-IND-PRODUCTO.
                    07 SL88A-IND-RTE-FTE         PIC X(01).
                    07 SL88A-IND-MANEJO          PIC X(01).
                    07 SL88A-IND-TIPO-PER        PIC X(01).
                    07 SL88A-ACT-ECON            PIC 9(02).
                    07 SL88A-IND-SEXO            PIC X(01).
                    07 SL88A-PER-EXTRACTO        PIC X(01).
                    07 SL88A-IND-ENV-EXT         PIC X(01).
                    07 SL88A-TALONARIO-INI       PIC 9(10).
                    07 SL88A-TALONARIO-FIN       PIC 9(10).
                 05 SL88A-DATOS-INTERES.
                    07 SL88A-TASA-INT            PIC V9(06).
                    07 SL88A-PLAZO               PIC 9(04).
                    07 SL88A-MOD-PAGO-INT        PIC X(01).
                    07 SL88A-PERIODO-RENDI       PIC 9(04).
                    07 SL88A-PERIODO-R   REDEFINES SL88A-PERIODO-RENDI.
                       09 SL88A-PERIODO          PIC X(04).
                 05 SL88A-TITULARES.
                    07 SL88A-TIPO-DCTO1          PIC X(01).
                    07 SL88A-DCTO1               PIC 9(13).
                    07 SL88A-NOMBRE1             PIC X(40).
                    07 SL88A-TIPO-DCTO2          PIC X(01).
                    07 SL88A-DCTO2               PIC 9(13).
                    07 SL88A-NOMBRE2             PIC X(40).
                    07 SL88A-TIPO-DCTO3          PIC X(01).
                    07 SL88A-DCTO3               PIC 9(13).
                    07 SL88A-NOMBRE3             PIC X(40).
                 05 SL88A-CORRESPONDENCIA.
                    07 SL88A-DIRECCION           PIC X(30).
                    07 SL88A-NOMBRE-CIUDAD       PIC X(15).
                    07 SL88A-CIUDAD              PIC 9(05).
                    07 SL88A-TELEFONO1           PIC 9(10).
                    07 SL88A-TELEFONO2           PIC 9(10).
                 05 SL88A-PRODUCTO-RELACIONADO.
                    07 SL88A-CUENTA              PIC 9(16).
                    07 SL88A-MOTIVO              PIC 9(02).
                    07 SL88A-TRJTA-DEBTO         PIC 9(16).
                    07 SL88A-CLAVE-ACTUAL        PIC X(16).
                    07 SL88A-CLAVE-ACTUAL-R REDEFINES
                       SL88A-CLAVE-ACTUAL.
                       08 FILLER                 PIC X(12).
                       08 SL88A-CLAVE-ACTUAL-4   PIC 9(4).
                    07 SL88A-CLAVE-NUEVA         PIC X(16).
                 05 SL88A-DATOS-PLAN-AVAL.
                    07 SL88A-CIUDAD-A            PIC 9(05).
                    07 SL88A-VLR-1               PIC 9(16)V99.
                    07 SL88A-VLR-2               PIC 9(16)V99.
                    07 SL88A-VLR-3               PIC 9(16)V99.
                 05 SL88A-EST-PROCESO            PIC X(1).
                 05 SL88A-FECHA-VCTO             PIC 9(08).
                 05 SL88A-FECHA-VCTO-R REDEFINES SL88A-FECHA-VCTO.
                    07 SL88A-ANO-VCTO            PIC 9(04).
                    07 SL88A-MES-VCTO            PIC 9(02).
                    07 SL88A-DIA-VCTO            PIC 9(02).
                 05 SL88A-DTOS-DOMICILIO.
                    07 SL88A-DIRECCION-CAS       PIC X(30).
                    07 SL88A-CIUDAD-CAS          PIC 9(05).
                    07 SL88A-TELEFONO-CAS        PIC 9(10).
                 05 SL88A-DTOS-OFICINA.
                    07 SL88A-DIRECCION-OFI       PIC X(30).
                    07 SL88A-APARTADO-OFI        PIC 9(06).
                    07 SL88A-CIUDAD-OFI          PIC 9(05).
                    07 SL88A-TELEFONO-OFI        PIC 9(10).
021223           05 SL88A-FEC-NAC                PIC 9(08).
030203           05 SL88A-TIPO-DCTO4             PIC X(01).
030203           05 SL88A-DCTO4                  PIC 9(13).
030929           05 SL88A-NOMBRE-TITULAR-4       PIC X(40).
030929           05 SL88A-INFORMACION-DOMICILIO.
030929              07 SL88A-NUMERO-DIRECCION    PIC  X.
030929              07 SL88A-NOMENCLATURA        PIC  X(4).
030929              07 SL88A-NUMERO1             PIC  X(30).
030929              07 SL88A-ORIENTACION-1       PIC  X(4).
030929              07 SL88A-NUMERO2             PIC  X(4).
030929              07 SL88A-ORIENTACION-2       PIC  X(4).
030929              07 SL88A-NUMERO3             PIC  X(7).
030929              07 SL88A-ORIENTACION-3       PIC  X(4).
030929              07 SL88A-ADICIONAL           OCCURS 4 TIMES.
030929                 09 SL88A-CODIGO-NOM       PIC  X(3).
030929                 09 SL88A-DETALLE          PIC  X(30).
030929              07 SL88A-TELEFONO-CRM        PIC  9(10).
030929              07 SL88A-BARRIO-CRM          PIC  X(20).
030929              07 SL88A-PAIS-CRM            PIC  X(3).
030929              07 SL88A-CIUDAD-CRM          PIC  9(5).
030929              07 SL88A-ESTADO-CRM          PIC  X(20).
030929              07 SL88A-APARTADO-AEREO-CRM  PIC  X(10).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 2 (CONSULTA)           *
      *---------------------------------------------------------------*
              03 SL88C-DATOS-CONSULTA  REDEFINES SL88-DATOS.
                 05 SL88C-DATOS-PRODUCTO-ORIGEN.
                    07 SL88C-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88C-COD-PROD-ORIG       PIC 9(02).
                    07 SL88C-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88C-PROD-ORIG-R REDEFINES SL88C-NRO-PROD-ORIG.
                       09 SL88C-PROD-ORIG        PIC 9(15).
                       09 SL88C-DIGITO           PIC 9(01).
                 05 SL88C-CONSEC-MOVIM           PIC 9(3).
                 05 SL88C-IND-TIPO-MOVIM         PIC X(16).
                 05 SL88C-REFERENCIA1            PIC 9(16).
080304           05 SL88C-REFERENCIA2            PIC 9(16).
080304           05 FILLER                       PIC X(796).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 2A (CONSULTA REVERSION *
      *        FORZADA)                                               *
      *---------------------------------------------------------------*
              03 SL88B-DATOS-CONSULTA  REDEFINES SL88-DATOS.
                 05 SL88B-SCNCIA-TRNSCCION       PIC 9(06).
                 05 SL88B-SCNCIA-DCMNTO          PIC 9(4).
                 05 SL88B-FNCNRIO-TMBRE          PIC X(13).
                 05 FILLER                       PIC X(844).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 3 (MOVIMIENTOS)        *
      *---------------------------------------------------------------*
              03 SL88M-DATOS-MOVTO         REDEFINES SL88-DATOS.
                 05 SL88M-DATOS-PRODUCTO-ORIGEN.
                    07 SL88M-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88M-COD-PROD-ORIG       PIC 9(02).
                    07 SL88M-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88M-PROD-ORIG-R REDEFINES SL88M-NRO-PROD-ORIG.
                       09 SL88M-PROD-ORIG        PIC 9(15).
                       09 SL88M-DIGITO           PIC 9(01).
                    07 SL88M-NRO-TRJTA           PIC 9(16).
                    07 SL88M-CLV-TRJTA           PIC 9(16).
                 05 SL88M-CONSEC-MOVIM           PIC 9(5).
                 05 SL88M-FECHA-INIC             PIC 9(8).
                 05 SL88M-FECHA-FIN              PIC 9(8).
                 05 SL88M-DATOS-ULT-REG          PIC X(60).
                 05 SL88M-DATOS-BL-CDT REDEFINES SL88M-DATOS-ULT-REG.
                    07 SL88M-FECHA-BLOQUEO       PIC 9(08).
                    07 SL88M-HORA-BLOQUEO        PIC 9(06).
                    07 SL88M-FILLER              PIC X(46).
020911           05 SL88M-DATOS-DEVOL  REDEFINES SL88M-DATOS-ULT-REG.
020911              07 SL88M-BCO-GIRADOR         PIC 9(2).
020911              07 SL88M-NRO-CHEQUE          PIC 9(12).
020911              07 SL88M-VLR-CHEQUE          PIC 9(16)V99.
020911              07 SL88M-SCIA                PIC 9(6).
020911              07 FILLER                    PIC X(22).
                 05 SL88M-ARCHIVO                PIC 9.
                 05 SL88M-IND-MOVIM              PIC 9.
020702           05 SL88M-TOT-DEB                PIC 9(16)V99.
020702           05 SL88M-TOT-CRE                PIC 9(16)V99.
020702           05 SL88M-SAL-FIN                PIC 9(16)V99.
                 05 SL88M-TIP-TRA                PIC X.
030702           05 SL88M-SIG-DEB                PIC X(1).
030702           05 SL88M-SIG-CRE                PIC X(1).
040223           05 SL88M-ISIN                   PIC X(12).
040223           05 FILLER                       PIC X(662).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 4 (CLIENTES)           *
      *---------------------------------------------------------------*
              03 SL88L-DATOS-APERTURAS REDEFINES SL88-DATOS.
                 05 SL88L-DATOS-PRODUCTO-ORIGEN.
                    07 SL88L-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88L-COD-PROD-ORIG       PIC 9(02).
                    07 SL88L-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88L-PROD-ORIG-R REDEFINES SL88L-NRO-PROD-ORIG.
                       09 SL88L-PROD-ORIG        PIC 9(15).
                       09 SL88L-DIGITO           PIC 9(01).
                    07 SL88L-COD-SUB-PROD        PIC X(01).
                 05 SL88L-IDENTIFICACION.
                    07 SL88L-TIP-DOCUMENTO       PIC  X(1).
                    07 SL88L-NUM-DOCUMENTO       PIC  9(13).
                 05 SL88L-INFORMACION-PERSONAL.
                    07 SL88L-NOMBRE-CLIENTE      PIC  X(40).
                    07 SL88L-LUG-EXPEDICION      PIC  9(5).
                    07 SL88L-FEC-EXPEDICION      PIC  9(8).
                    07 SL88L-LUG-NACIMIENTO      PIC  9(5).
                    07 SL88L-FEC-NACIMIENTO      PIC  9(8).
                    07 SL88L-SEXO                PIC  X(1).
                    07 SL88L-EST-CIVIL           PIC  X(1).
                    07 SL88L-NUM-HIJOS           PIC  9(2).
                    07 SL88L-NIV-ESTUDIOS        PIC  X(1).
                 05 SL88L-INFORMACION-DOMICILIO.
                    07 SL88L-CODIGO-DIR-CLI      PIC  X(3).
                    07 SL88L-DIRECCION-CLI       PIC  X(27).
                    07 SL88L-TELEFONO-CLI        PIC  9(10).
                    07 SL88L-APARTADO-CLI        PIC  9(6).
                    07 SL88L-FAX-CLI             PIC  9(10).
                    07 SL88L-EMAIL-CLI           PIC  X(25).
                    07 SL88L-CIUDAD-CLI          PIC  9(5).
                 05 SL88L-INFORMACION-OCUPACION.
                    07 SL88L-OCUPACION           PIC  9(4).
                    07 SL88L-NOMBRE-EMP          PIC  X(40).
                    07 SL88L-CODIGO-DIR-EMP      PIC  X(3).
                    07 SL88L-DIRECCION-EMP       PIC  X(27).
                    07 SL88L-FAX-EMP             PIC  9(10).
                    07 SL88L-APARTADO-EMP        PIC  9(6).
                    07 SL88L-TELEFONO-EMP        PIC  9(10).
                    07 SL88L-NUMERO-EXT-EMP      PIC  9(4).
                    07 SL88L-CIUDAD-EMP          PIC  9(5).
                 05 SL88L-INFORMACION-FINANCIERA.
                    07 SL88L-IND-CASA            PIC  X(1).
                    07 SL88L-IND-VEHICULO        PIC  X(1).
                    07 SL88L-VLR-INGRESOS        PIC  9(16)V99.
                    07 SL88L-VLR-EGRESOS         PIC  9(16)V99.
                    07 SL88L-TOT-ACTIVOS         PIC  9(16)V99.
                    07 SL88L-TOT-PASIVOS         PIC  9(16)V99.
                 05 SL88L-INF-REFERENCIA-FAMILIAR.
                    07 SL88L-NOMBRE-FAM          PIC  X(40).
                    07 SL88L-IND-PARENTESCO      PIC  X(1).
                    07 SL88L-NUM-DOCUMENTO-REP   PIC  9(13).
                    07 SL88L-TELEFONO-FAM        PIC  9(10).
                    07 SL88L-CODIGO-DIR-FAM      PIC  X(3).
                    07 SL88L-DIRECCION-FAM       PIC  X(27).
                    07 SL88L-CIUDAD-FAM          PIC  9(5).
                 05 SL88L-INF-REFERENCIA-PERSONAL.
                    07 SL88L-NOMBRE-PER          PIC  X(40).
                    07 SL88L-TELEFONO-PER        PIC  9(10).
                    07 SL88L-CODIGO-DIR-PER      PIC  X(3).
                    07 SL88L-DIRECCION-PER       PIC  X(27).
                    07 SL88L-CIUDAD-PER          PIC  9(5).
                 05 SL88L-OPERACION-INT          PIC  X(1).
                 05 SL88L-IND-REFERIDO           PIC  X(1).
030915           05 SL88L-IND-REC-ESTADO         PIC  X(1).
030915           05 SL88L-DIG-CHEQUEO            PIC  9(1).
      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 5 (FIRMAS AVAL  Y      *
      *                                          (CONDICIONES MANEJO) *
      *---------------------------------------------------------------*
              03 SL88O-DATOS-MOVTO         REDEFINES SL88-DATOS.
                 05 SL88O-DATOS-PRODUCTO-ORIGEN.
                    07 SL88O-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88O-COD-PROD-ORIG       PIC 9(02).
                    07 SL88O-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88O-PROD-ORIG-R REDEFINES SL88O-NRO-PROD-ORIG.
                       09 SL88O-PROD-ORIG        PIC 9(15).
                       09 SL88O-DIGITO           PIC 9(01).
                 05 SL88O-DATOS-REG              PIC X(813).
                 05 SL88O-DATOS-FIRMAS REDEFINES SL88O-DATOS-REG.
                    07 SL88O-LLAVE-ULT-REG.
                       09 SL88O-IDE-FIRMA     PIC  X(02).
                       09 SL88O-LONG-FIRMA    PIC  X(04).
021001                 09 SL88O-NRO-CHEQUE    PIC  9(10).
                       09 FILLER              PIC  X(34).
                    07 FILLER                 PIC  X(763).
                 05 SL88O-DATOS-CONMANE REDEFINES SL88O-DATOS-REG.
                    07 SL88O-TIPO-NOVEDAD     PIC  9(1).
                    07 SL88O-NOMBRE           PIC  X(40).
                    07 SL88O-NIT              PIC  9(13).
                    07 SL88O-COND-MANEJO      PIC  X(350).
                    07 FILLER                 PIC  X(409).

      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 6 (EXTRACTOS TC)       *
      *---------------------------------------------------------------*
              03 SL88E-DATOS-EXTRACTO REDEFINES SL88-DATOS.
                 05 SL88E-DATOS-PRODUCTO-ORIGEN.
                    07 SL88E-ENT-ORI            PIC 9(2).
                    07 SL88E-PRO-ORI            PIC 9(2).
                    07 SL88E-NUM-PRO            PIC 9(16).
                    07 SL88E-CLA-TAR            PIC X(16).
                    07 SL88E-CON-MOV            PIC 9(5).
                    07 SL88E-FEC-MOV            PIC 9(4).
                    07 SL88E-LLA-ULT            PIC X(12).
                 05 FILLER                      PIC X(810).
030212*---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 9 SEGM. CLIENTES      *
      *---------------------------------------------------------------*
              03 SL88K-DATOS-SEGMENTO  REDEFINES SL88-DATOS.
                 05 SL88K-DATOS-PRODUCTO-ORIGEN.
                    07 SL88K-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88K-COD-PROD-ORIG       PIC 9(02).
                    07 SL88K-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88K-PROD-ORIG-R  REDEFINES SL88K-NRO-PROD-ORIG.
                       09 SL88K-PROD-ORIG        PIC 9(15).
                       09 SL88K-DIGITO           PIC 9(1).
                 05 SL88K-TIPO-OPERACION         PIC X(1).
                 05 SL88K-ARREGLO  OCCURS 12 TIMES.
                    07 SL88K-EST-PRODUCTO        PIC X(2).
030212           05 FILLER                       PIC X(500).
030708*---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA 10 (CLIENTES -CRM)     *
      *---------------------------------------------------------------*
              03 SL88R-DATOS-CLIENTES REDEFINES SL88-DATOS.
                 05 SL88R-DATOS-PRODUCTO-ORIGEN.
                    07 SL88R-ENTIDAD-ORIG        PIC 9(02).
                    07 SL88R-COD-PROD-ORIG       PIC 9(02).
                    07 SL88R-NRO-PROD-ORIG       PIC 9(16).
                    07 SL88R-PROD-ORIG-R REDEFINES SL88R-NRO-PROD-ORIG.
                       09 SL88R-PROD-ORIG        PIC 9(15).
                       09 SL88R-DIGITO           PIC 9(01).
                    07 SL88R-COD-SUB-PROD        PIC X(01).
                 05 SL88R-DATOS.
                    07 SL88R-IDENTIFICACION.
                       09 SL88R-TIP-DOCUMENTO       PIC  X(1).
                       09 SL88R-NUM-DOCUMENTO       PIC  9(12).
                       09 SL88R-DIG-CHEQUEO         PIC  9(1).
                       09 SL88R-CIUDAD-DOCUMENTO    PIC  9(5).
                    07 SL88R-INFORMACION-PERSONAL.
                       09 SL88R-TIPO-PERSONA        PIC  X(1).
                       09 SL88R-NOMBRE-1            PIC  X(20).
                       09 SL88R-NOMBRE-2            PIC  X(20).
                       09 SL88R-APELLIDO-1          PIC  X(20).
                       09 SL88R-APELLIDO-2          PIC  X(20).
                       09 SL88R-FEC-NACIMIENTO      PIC  9(8).
                       09 SL88R-SEXO                PIC  X(1).
                    07 SL88R-INFORMACION-DOMICILIO.
                       09 SL88R-NOMENCLATURA        PIC  X(4).
                       09 SL88R-NUMERO1             PIC  X(30).
                       09 SL88R-ORIENTACION-1       PIC  X(4).
                       09 SL88R-NUMERO2             PIC  X(4).
                       09 SL88R-ORIENTACION-2       PIC  X(4).
                       09 SL88R-NUMERO3             PIC  X(7).
                       09 SL88R-ORIENTACION-3       PIC  X(4).
                       09 SL88R-ADICIONAL           OCCURS 4 TIMES.
                          11 SL88R-CODIGO-NOM       PIC  X(3).
                          11 SL88R-DETALLE          PIC  X(30).
                       09 SL88R-ZONA                PIC  X(3).
                       09 SL88R-DIRECCION-RURAL     PIC  X(40).
                       09 SL88R-TELEFONO            PIC  9(10).
                       09 SL88R-BARRIO              PIC  X(20).
                       09 SL88R-PAIS                PIC  X(3).
                       09 SL88R-CIUDAD              PIC  9(5).
                       09 SL88R-ESTADO              PIC  X(20).
                       09 SL88R-APARTADO-AEREO      PIC  X(10).
                    07 SL88R-INFORMACION-OCUPACION.
                       09 SL88R-OCUPACION           PIC  9(2).
                    07 SL88R-INFORMACION-FINANCIERA.
                       09 SL88R-VLR-INGRESOS        PIC  9(18).
                    07 SL88R-ADICIONAL-JURIDICAS.
                       09 SL88R-RAZON-SOCIAL        PIC  X(40).
                       09 SL88R-TIPO-DOCU-REP       PIC  X(1).
                       09 SL88R-NUMERO-DOCU-REP     PIC  9(13).
                       09 SL88R-NOMBRE-REP-1        PIC  X(20).
                       09 SL88R-NOMBRE-REP-2        PIC  X(20).
                       09 SL88R-APELLIDO-REP-1      PIC  X(20).
                       09 SL88R-APELLIDO-REP-2      PIC  X(20).
                       09 SL88R-COD-CIUU-1N         PIC  9(2).
                       09 SL88R-COD-CIUU-2N         PIC  9(2).
                    07 SL88R-ACT-ECO-DETALLADA      PIC  X(50).
                    07 SL88R-IND-REC-ESTADO         PIC  X(1).
                    07 SL88R-IND-REL-BANCO          PIC  X(5).
                    07 FILLER                       PIC X(200).
                 05 SL88R-DATOS-CON REDEFINES SL88R-DATOS.
                    07 SL88R-IDENTIFICA.
                       09 SL88R-TIP-DOCU       PIC  X(1).
                       09 SL88R-NUM-DOCU       PIC  9(12).
                       09 SL88R-DIG-CHEQ1      PIC  9(1).
                       09 SL88R-TIP-PERS-CLI   PIC  X(1).
                       09 SL88R-TIP-DOC-REP    PIC  X(1).
                       09 SL88R-NUM-DOC-REP    PIC  9(13).
                    07 FILLER                  PIC X(400).
      *---------------------------------------------------------------*
      *        DATOS MENSAJERIA ADMINISTRATIVA                        *
      *---------------------------------------------------------------*